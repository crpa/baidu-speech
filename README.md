#### 介绍
在线百度语音播报

#### 软件架构

Python3.7.2、Django2.1.7、baidu-aip

#### 界面展示

![输入图片说明](https://images.gitee.com/uploads/images/2019/0308/220016_723eb90b_87650.png "OGPAY%M84{@{FSRIVH6AC4O.png")


#### 安装部署

- [本地运行测试](https://gitee.com/52itstyle/baidu-speech/blob/master/readme/%E6%9C%AC%E5%9C%B0%E8%BF%90%E8%A1%8C%E6%B5%8B%E8%AF%95.md)

- [Linux下安装部署](https://gitee.com/52itstyle/baidu-speech/blob/master/readme/Linux%E4%B8%8B%E5%AE%89%E8%A3%85%E9%83%A8%E7%BD%B2.md)

#### 移动端

![输入图片说明](https://images.gitee.com/uploads/images/2019/0312/203604_2b311610_87650.png "二维码美化.png")


#### 从零学Python

https://gitee.com/52itstyle/Python