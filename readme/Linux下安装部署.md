## 安装教程

#### 安装 Python3


```
wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tar.xz
```
事先安装依赖，否则后期安装会报错：

```
yum -y install zlib*
yum -y install libffi-devel
```
下面开始正式安装：

```
# 解压
tar -xvf Python-3.7.1.tar.xz
# 切换大目录
cd Python-3.7.1
# 配置编译
./configure
# 编译安装
make && make install
```
#### 安装 Django


```
pip install Django
```
安装成功以后需要重新配置并编译安装 Python3：

```
# 配置编译
./configure
# 编译安装
make && make install
```
##### 安装服务器 uwsgi


```
pip3 install uwsgi
```

上传项目到服务器，并切换到 speech 目录：

```
# 目录下新建文件夹
mkdir script
```
在 script 下新增 uwsgi.ini (项目中已经配置好，自行修改路径即可)：

```


# uwsig使用配置文件启动
[uwsgi]
# 项目目录
chdir=/www/speech/
# 指定项目的application
module=speech.wsgi:application
# 指定sock的文件路径       
socket=/www/speech/script/uwsgi.sock
# 进程个数       
workers=5
pidfile=/www/speech/script/uwsgi.pid
# 指定IP端口       
http=127.0.0.1:8001
# 指定静态文件
static-map=/static=/www/speech/static
# 启动uwsgi的用户名和用户组
uid=root
gid=root
# 启用主进程
master=true
# 自动移除unix Socket和pid文件当服务停止的时候
vacuum=true
# 序列化接受的内容，如果可能的话
thunder-lock=true
# 启用线程
enable-threads=true
# 设置自中断时间
harakiri=30
# 设置缓冲
post-buffering=4096
# 设置日志目录
daemonize=/www/speech/script/uwsgi.log

```

然后使用以下命令启动：

```
uwsgi  --ini uwsgi.ini
```
执行命令，查看是否启动成功：

```
[root@AY140216131049Z script]# ps -ef|grep uwsgi  
root      3040     1  0 Nov21 ?        00:00:03 uwsgi --ini uwsgi.ini
root      3041  3040  0 Nov21 ?        00:00:00 uwsgi --ini uwsgi.ini
root      3042  3040  0 Nov21 ?        00:00:00 uwsgi --ini uwsgi.ini
root      3043  3040  0 Nov21 ?        00:00:00 uwsgi --ini uwsgi.ini
root      3044  3040  0 Nov21 ?        00:00:00 uwsgi --ini uwsgi.ini
root      3045  3040  0 Nov21 ?        00:00:00 uwsgi --ini uwsgi.ini
root      3046  3040  0 Nov21 ?        00:00:00 uwsgi --ini uwsgi.ini
root      6606  6580  0 18:13 pts/0    00:00:00 grep --color=auto uwsgi
```

重启：

```
uwsgi --reload uwsgi.pid
```

##### 配置Nginx代理：


```

server {
        listen  80;
        server_name  speech.52itstyle.vip;
        charset utf-8;
        location / {
           include uwsgi_params; # 导入一个Nginx模块他是用来和uWSGI进行通讯的
           uwsgi_connect_timeout 30; # 设置连接uWSGI超时时间
           uwsgi_pass unix:/www/speech/script/uwsgi.sock; # 指定uwsgi的sock文件所有动态请求就会直接丢给他
        }
        # 动静分离 Nginx 处理静态请求
        location /static {
            root /www/speech/;
        }
                           
   }

```
如果启动HTTPS：

```


server {
         listen 80;
         listen 443 ssl;
         server_name  speech.52itstyle.vip;
         #ssl on;
         #证书路径
         ssl_certificate    /usr/local/openresty/nginx/cert/1901523_speech.52itstyle.vip.pem;
         #私钥路径
         ssl_certificate_key   /usr/local/openresty/nginx/cert/1901523_speech.52itstyle.vip.key;
         #缓存有效期
         ssl_session_timeout 5m;
         #可选的加密算法,顺序很重要,越靠前的优先级越高.
         ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
         #安全链接可选的加密协议
         ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
         ssl_prefer_server_ciphers on;

        location / {
          #  proxy_pass http://127.0.0.1:8000;
           include uwsgi_params; # 导入一个Nginx模块他是用来和uWSGI进行通讯的
           uwsgi_connect_timeout 30; # 设置连接uWSGI超时时间
           uwsgi_pass unix:/www/speech/script/uwsgi.sock; # 指定uwsgi的sock文件所有动态请求就会直接丢给他
        }
        # 动静分离 Nginx 处理静态请求
        location /static {
            root /www/speech/;
        }
                           
   }

```

## 参考

https://blog.52itstyle.vip/archives/3474/

https://blog.52itstyle.vip/archives/3503/